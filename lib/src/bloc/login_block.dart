
import 'dart:async';

import 'package:form_bloc/src/bloc/validators.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/streams.dart';

class LoginBloc with Validators{

  final _emailController    = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  

  //recuperar datos del stream
  Stream<String> get emailStream     => _emailController.stream.transform( validarEmail );
  Stream<String> get passwordStream  => _passwordController.stream.transform( validarPassword );

  Stream<bool> get formValidStream => 
    CombineLatestStream.combine2(emailStream, passwordStream, (e, p) => true);

  //insertar datos
  Function(String) get getchangeEmail => _emailController.sink.add;
  Function(String) get getchangePassword => _passwordController.sink.add;

  //obtener ultimo valor ingresado a los streams
  String get email    => _emailController.value;
  String get password => _passwordController.value;


  dispose(){
    _emailController   ?.close();
    _passwordController?.close();
  }

}